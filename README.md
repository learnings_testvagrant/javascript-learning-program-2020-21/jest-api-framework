# Javascript-Final-Phase

​This project is a API test automation framework using JS.

# Tech stack

Package management - npm <br/>
Framework - jest <br/>
Http library - supertest <br/>
Assertion library - jest <br/>
Reporter - jest-html-reporter <br/>

# Features

1. Parallel execution
2. CI/CD intergration
3. Slack notification
4. Jest reporting
5. Jest snapshot testing


_Let's get started ..._

1. Clone the project. <br/>
  `git clone https://gitlab.com/snidhi/javascript-final-phase.git `
   
2. Open the project in VS Code or navigate to project directory in terminal. <br/>
   `cd javascript-final-phase`

3. Install the dependencies by running the command. <br/>
   `npm install`

4. Once the dependencies have been installed, run the tests using. <br/>
   `jest` or `npm test` 



