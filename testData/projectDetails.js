module.exports = {
    projectDetails: [
        {
            name: 'Jest-At-It-Best',
            description: 'This was fun!'
        },
        {
            name: 'Jest-E2E-Project',
            description: 'End to end project'
        }
    ],

    variable: {
        key: "KEY",
        value: "I'm your value"
    },

    projectID: "23603350"
}