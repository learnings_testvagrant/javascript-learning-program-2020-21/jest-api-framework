const { baseURL, token } = require('../../resources/env');
const supertest = require('supertest')
const request = supertest(baseURL)
const { project } = require('../../resources/endPoints');
const { projectDetails } = require('../../testData/projectDetails');

describe('Project', () => {

    const headers = {
        'PRIVATE-TOKEN': token
    }

    let projectId
    it('should create a new gitlab private project', async () => {

        const response = await request.post(project)
            .set(headers)
            .send(projectDetails[0])

        expect(response.statusCode).toBe(201);
        expect(response.body.name).toBe(projectDetails[0].name)
        console.log("Project ID : ", response.body.id);
        console.log("Project Name : ", response.body.name);
        projectId = response.body.id;
    });

    it('should fetch a private project', async () => {

        const response = await request.get(`${project}/${projectId}`)
            .set(headers)

        expect(response.statusCode).toBe(200);
        expect(response.body.id).toBe(projectId)
        expect(response.body.name).toBe(projectDetails[0].name)
    });

    it('should not be able to create an already existing project', async () => {

        await request.post(project)
            .set(headers)
            .send(projectDetails[0])
            .then(res => {
                expect(res.statusCode).toBe(400);
                expect(res.text).toContain("has already been taken")
                console.log(res.text)
            })
    });

    it('should delete the project', async () => {

        const response = await request.delete(`${project}/${projectId}`)
            .set(headers);

        expect(response.statusCode).toBe(202);
        console.log(response.body);
    });

    it('should not be able to delete non existing project', async () => {

        const response = await request.delete(`${project}/${projectId}`)
            .set(headers);

        expect(response.statusCode).toBe(404);
        expect(response.body.message).toBe("404 Project Not Found");
        console.log(response.body);
    });
});