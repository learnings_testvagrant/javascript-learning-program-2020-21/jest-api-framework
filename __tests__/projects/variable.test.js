const { baseURL, token } = require('../../resources/env');
const supertest = require('supertest')
const request = supertest(baseURL)
const { project } = require('../../resources/endPoints');
const { variable, projectID } = require('../../testData/projectDetails');

describe('Variables', () => {

    const headers = {
        'PRIVATE-TOKEN': token
    }

    it('should add a variable to the project ', async () => {
        const response = await request.post(`${project}/${projectID}/variables`)
            .set(headers)
            .send(variable);

        expect(response.statusCode).toBe(201);
        console.log(response.body);
    });

    it('should not be able to add an already existing variable to the project ', async () => {
        const response = await request.post(`${project}/${projectID}/variables`)
            .set(headers)
            .send(variable);

        expect(response.statusCode).toBe(400);
        expect(response.body.message.key).toContain(`(${variable.key}) has already been taken`);
        console.log(response.body);
    });

    it('should remove variable from the project', async () => {
        const response = await request.delete(`${project}/${projectID}/variables/${variable.key}`)
            .set(headers);

        expect(response.statusCode).toBe(204);
        expect(response.body).toEqual({})
        console.log(response.body);
    });

    it('should not be able to remove non existing variable', async () => {
        const response = await request.delete(`${project}/${projectID}/variables/${variable.key}`)
            .set(headers);

        expect(response.statusCode).toBe(404);
        expect(response.body.message).toBe("404 Variable Not Found");
        console.log(response.body);
    });

});