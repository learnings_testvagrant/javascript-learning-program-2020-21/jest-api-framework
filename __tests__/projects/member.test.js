const { baseURL, token } = require('../../resources/env');
const supertest = require('supertest')
const request = supertest(baseURL)
const { project } = require('../../resources/endPoints');
const { projectID } = require('../../testData/projectDetails');
const { userDetails } = require('../../testData/userDetails');

describe('Members', () => {

    const headers = {
        'PRIVATE-TOKEN': token
    }

    it('should add a member to project as developer', async () => {

        const response = await request.post(`${project}/${projectID}/members`)
            .set(headers)
            .query(userDetails);
            

        expect(response.statusCode).toBe(201)
        console.log(response.body);
    });

    it('should be able to fetch all members of a project', async () => {

        const response = await request.get(`${project}/${projectID}/members`)
            .set(headers)
            .query(userDetails);

        expect(response.statusCode).toBe(200);
        console.log(response.body);
    });

    it('should not be able to add an already existing member to the project', async () => {

        const response = await request.post(`${project}/${projectID}/members`)
            .set(headers)
            .query(userDetails);

        expect(response.statusCode).toBe(409);
        expect(response.body.message).toBe("Member already exists");
        console.log(response.body);
    });

    it('should remove the member from the project', async () => {

        const response = await request.delete(`${project}/${projectID}/members/${userDetails.user_id}`)
            .set(headers)
            .query(userDetails);

        expect(response.statusCode).toBe(204);
        expect(response.body).toEqual({});
    });
});