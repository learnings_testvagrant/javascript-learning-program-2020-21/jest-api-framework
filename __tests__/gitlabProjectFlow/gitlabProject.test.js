const { baseURL, token } = require('../../resources/env');
const supertest = require('supertest')
const request = supertest(baseURL);
const { project } = require('../../resources/endPoints');
const { projectDetails, variable } = require('../../testData/projectDetails');
const { userDetails } = require('../../testData/userDetails');

describe('E2E Project', () => {

    const headers = {
        'PRIVATE-TOKEN': token
    }

    let projectId

    it('should create a new gitlab private project', async () => {

        const response = await request.post(project)
            .set(headers)
            .send(projectDetails[1]);

        expect(response.statusCode).toBe(201);
        expect(response.body.description).toMatchInlineSnapshot('"End to end project"');
        projectId = response.body.id;

    });

    it('should add a member to project as developer', async () => {

        const response = await request.post(`${project}/${projectId}/members`)
            .set(headers)
            .query(userDetails);

        expect(response.statusCode).toBe(201);
    });

    it('should add a variable to the project and delete it', async () => {

        const createVariableResponse = await request.post(`${project}/${projectId}/variables`)
            .set(headers)
            .send(variable);

        expect(createVariableResponse.statusCode).toBe(201);

        const deleteVariableResponse = await request.delete(`${project}/${projectId}/variables/${variable.key}`)
            .set(headers);

        expect(deleteVariableResponse.statusCode).toBe(204);
    });

    it('should delete the project', async () => {

        const response = await request.delete(`${project}/${projectId}`)
            .set(headers);

        expect(response.statusCode).toBe(202);
    });
});