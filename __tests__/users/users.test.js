const { baseURL, token } = require('../../resources/env');
const supertest = require('supertest')
const request = supertest(baseURL)
const { user } = require('../../resources/endPoints');
const { userId } = require('../../testData/userDetails');

describe('User', () => {

    const headers = {
        'PRIVATE-TOKEN': token
    }

    test.concurrent('should be able to get userId', async () => {
        const response = await request.get(`${user}/${userId}`)
            .set(headers)

        expect(response.statusCode).toBe(200);
        expect(response.body).toMatchSnapshot()
        console.log(response.body);
    });

    test.concurrent('should be able to get the projects of the user', async () => {
        const response = await request.get(`${user}/${userId}/projects`)
            .set(headers)

        expect(response.statusCode).toBe(200);
        response.body.forEach(project => {
            console.log(project.name)
        });

    });

    test.concurrent('should be able to get emails of the user', async () => {
        const response = await request.get(`/user/emails`)
            .set(headers);

        expect(response.statusCode).toBe(200);
        console.log(response.body);
    });
});